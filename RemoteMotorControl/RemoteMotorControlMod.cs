﻿using FMODUnity;
using HarmonyLib;
using System.Collections;
using UnityEngine;

namespace RemoteMotorControl
{
    public class RemoteMotorControlMod : Mod
    {
        public IEnumerator Start()
        {
            var bundleLoadRequest = AssetBundle.LoadFromMemoryAsync(GetEmbeddedFileBytes("motorcontrol.assets"));
            yield return bundleLoadRequest;

            AssetBundle assetBundle = bundleLoadRequest.assetBundle;
            if (assetBundle == null)
            {
                LogError("Failed to load Asset Bundle. This mod will not work.");
                yield return null;
            }

            Item_Base motorControl = assetBundle.LoadAsset<Item_Base>("Placeable_MotorControl_Item");

            var gameObject = motorControl.settings_buildable.GetBlockPrefab(0).gameObject;
            gameObject.AddComponent<MotorControl>();

            // register item
            RAPI.AddItemToBlockQuadType(motorControl, RBlockQuadType.quad_floor);
            RAPI.AddItemToBlockQuadType(motorControl, RBlockQuadType.quad_foundation);
            RAPI.RegisterItem(motorControl);

            Info("Mod was loaded successfully!");
            FollowUpLog("This mod is still in development, please make a backup before you use it!");
        }

        public void OnModUnload()
        {
            Debug.Log("Mod RemoteMotorControl has been unloaded!");
        }

        private static void Info(string message)
        {
            Debug.Log("<color=#3498db>[info]</color>\t<b>RemoteMotorControl:</b> " + message);
        }

        private static void FollowUpLog(string message)
        {
            Debug.Log("\t" + message);
        }

        private static void LogError(string message)
        {
            Debug.LogError("<color=#e74c3c>[error]</color>\t<b>RemoteMotorControl:</b> " + message);
        }
    }
}
