﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace RemoteMotorControl
{
    public class LedBlock
    {
        private readonly Dictionary<Color, GameObject> _leds;
        public LedBlock()
        {
            this._leds = new Dictionary<Color, GameObject>();
        }

        public void AddLed(Color color, GameObject led)
        {
            if (led == null) throw new ArgumentNullException(nameof(led));
            if (this._leds.ContainsKey(color)) throw new InvalidOperationException($"LedBlock already has attached led for {color} color");

            this._leds.Add(color, led);
        }

        public void RemoveLed(Color color)
        {
            this._leds.Remove(color);
        }

        public void TurnOff()
        {
            foreach (var led in this._leds)
            {
                led.Value.SetActive(false);
            }
            if (this._leds.TryGetValue(Color.Off, out GameObject ledOff))
            {
                ledOff.SetActive(true);
            }
        }

        public void TurnOn(Color color)
        {
            if (!this._leds.ContainsKey(color)) throw new InvalidOperationException($"LedBlock doesn't contain led for {color} color");
            foreach (var led in this._leds)
            {
                led.Value.SetActive(led.Key == color);
            }
        }

        public enum Color
        {
            Off,
            Green,
            Red
        }
    }
}
