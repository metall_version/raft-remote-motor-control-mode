﻿using HarmonyLib;
using Steamworks;
using System;
using System.Linq;
using UnityEngine;

namespace RemoteMotorControl
{
    public class MotorControl : MonoBehaviour_ID_Network
    {
        private BlockButton _buttonStart;
        private BlockButton _buttonStop;
        private LedBlock _ledMotor;

        private BlockButton _buttonForward;
        private BlockButton _buttonBack;
        private LedBlock _ledForward;
        private LedBlock _ledBack;

        private BlockButton _buttonAnchor;
        private LedBlock _ledAnchor;
        private Animator _anchorAnimator;

        private Battery _battery;
        private int _batteryEnergyUses = 1;
        private GameObject _batteryWires;

        private Lazy<Semih_Network> _network = new Lazy<Semih_Network>(() => ComponentManager<Semih_Network>.Value);

        // current states
        private bool _turnedOn;
        private bool _isMotorActive;
        private bool _isAnchorAtBottom;
        private MotorDirection _currentDirection;
        private bool _hasBeenPlaced;

        void Start()
        {
            Info($"MotorControl.Start {DateTime.Now}");
        }

        private void ButtonStartPressedHandler()
        {
            RunButtonAnimation(this._buttonStart);

            this.UpdateMotorState(active: true);
        }

        private void ButtonStopPressedHandler()
        {
            RunButtonAnimation(this._buttonStop);

            this.UpdateMotorState(active: false);
        }

        private void ButtonForwardPressedHandler()
        {
            RunButtonAnimation(this._buttonForward);

            this.UpdateDirection(MotorDirection.Forward);
        }

        private void ButtonBackPressedHandler()
        {
            RunButtonAnimation(this._buttonBack);

            this.UpdateDirection(MotorDirection.Back);
        }

        private void ButtonAnchorPressedHandler()
        {
            this.ToogleAnchorState();
        }

        private static void RunButtonAnimation(BlockButton button)
        {
            button.GetComponent<Animator>().SetTrigger("Press");
        }

        public void OnBlockPlaced()
        {
            Info($"MotorControl.OnBlockPlaced {DateTime.Now}");

            NetworkIDManager.AddNetworkID(this);
            this._hasBeenPlaced = true;

            this.InitializeGameObjects();
            this.Initialize();
        }

        private void Update()
        {
            if (!this._hasBeenPlaced)
            {
                return;
            }

            if (Semih_Network.IsHost)
            {
                bool isBatteryPlaced = !this._battery.BatterySlotIsEmpty;
                if (isBatteryPlaced != this._batteryWires.activeSelf)
                {
                    this._batteryWires.SetActive(isBatteryPlaced);
                }

                if (this._turnedOn != this._battery.CanGiveElectricity)
                {
                    this.HandleBatteryUpdate();
                }
            }
        }

        protected override void OnDestroy()
        {
            Info($"MotorControl.OnDestroy {DateTime.Now}");

            base.OnDestroy();

            if (this._hasBeenPlaced)
            {
                if (!this._battery.BatterySlotIsEmpty)
                {
                    var localPlayer = RAPI.GetLocalPlayer();
                    this._battery.Take(localPlayer, this._battery.BatteryUses);
                }

                NetworkIDManager.RemoveNetworkID(this);
            }
        }

        private void InitializeGameObjects()
        {
            // buttons
            var buttons = GetComponentsInChildren<BlockButton>();

            this._buttonStart = buttons.Single(x => x.name == "Button_Start");
            this._buttonStop = buttons.Single(x => x.name == "Button_Stop");
            this._buttonForward = buttons.Single(x => x.name == "Button_Forward");
            this._buttonBack = buttons.Single(x => x.name == "Button_Back");
            this._buttonAnchor = buttons.Single(x => x.name == "Button_Anchor");

            this._buttonStart.OnButtonPressed = this.ButtonStartPressedHandler;
            this._buttonStop.OnButtonPressed = this.ButtonStopPressedHandler;
            this._buttonForward.OnButtonPressed = this.ButtonForwardPressedHandler;
            this._buttonBack.OnButtonPressed = this.ButtonBackPressedHandler;
            this._buttonAnchor.OnButtonPressed = this.ButtonAnchorPressedHandler;

            // anchor
            this._anchorAnimator = this._buttonAnchor.GetComponentInParent<Animator>();

            // battery
            this._battery = base.GetComponentInChildren<Battery>();
            Traverse.Create(this._battery).Field("networkBehaviourID").SetValue(this);
            this._batteryWires = this.transform.FindChildRecursively("BatteryCharger_Part").gameObject;

            // leds
            this._ledMotor = new LedBlock();
            this._ledForward = new LedBlock();
            this._ledBack = new LedBlock();
            this._ledAnchor = new LedBlock();

            var container = this.transform.FindChildRecursively("ControlPanel");
            var lightMotorBlock = container.transform.Find("Light_Motor").gameObject;
            this.AttachAllLedColors(this._ledMotor, lightMotorBlock);

            var lightForwardBlock = container.transform.Find("Light_Forward").gameObject;
            this.AttachAllLedColors(this._ledForward, lightForwardBlock);

            var lightBackBlock = container.transform.Find("Light_Back").gameObject;
            this.AttachAllLedColors(this._ledBack, lightBackBlock);

            var lightAnchorBlock = container.transform.Find("Light_Anchor").gameObject;
            this.AttachAllLedColors(this._ledAnchor, lightAnchorBlock);
        }

        private void HandleBatteryUpdate()
        {
            if (this._turnedOn == this._battery.CanGiveElectricity) return;

            this._turnedOn = this._battery.CanGiveElectricity;

            this.RefreshMotorLed();
            this.RefreshDirectionLed();
            this.RefreshAnchorLed();
            this.InitializeAnchorLeverPosition();

            if (!this._turnedOn)
            {
                this._isMotorActive = false;
                this.StopAllMotors();
            }
        }

        private void AttachAllLedColors(LedBlock led, GameObject lightBlock)
        {
            led.AddLed(LedBlock.Color.Off, lightBlock.transform.Find("Light_Off").gameObject);
            led.AddLed(LedBlock.Color.Green, lightBlock.transform.Find("Light_Green").gameObject);
            led.AddLed(LedBlock.Color.Red, lightBlock.transform.Find("Light_Red").gameObject);
        }

        private void Initialize()
        {
            this._turnedOn = false;
            this._isMotorActive = false;
            this._currentDirection = MotorDirection.Forward;

            this.InitializeAnchorLeverPosition();

            if (this._battery.CanGiveElectricity)
            {
                this._turnedOn = true;
                this._isMotorActive = this.DoAllMotorsActive();
            }
            else
            {
                this.StopAllMotors();
            }

            this.RefreshMotorLed();
            this.RefreshDirectionLed();
            this.RefreshAnchorLed();
        }

        private void InitializeAnchorLeverPosition()
        {
            this._isAnchorAtBottom = this.DoesAnyAnchorAtBottom();

            bool isCurrentLeverDown = this._anchorAnimator.GetBool("AtBottom");
            if (isCurrentLeverDown != this._isAnchorAtBottom)
            {
                this._anchorAnimator.SetBool("AtBottom", this._isAnchorAtBottom);
            }
        }

        private void UpdateMotorState(bool active)
        {
            if (active == this._isMotorActive || !this._turnedOn) return;

            this._isMotorActive = active;
            this.RefreshMotorLed();
            this.RefreshDirectionLed();
            this.SetStateForAllMotors(active);

            this.UseBatteryEnergy();
        }

        private void UpdateDirection(MotorDirection direction)
        {
            if (direction == this._currentDirection || !_turnedOn) return;

            this._currentDirection = direction;
            this.RefreshDirectionLed();
            this.InverseDirectionForAllMotors();

            this.UseBatteryEnergy();
        }

        private void ToogleAnchorState()
        {
            if (!this._turnedOn) return;

            if (!this.CanAllAnchorsBeUsed())
            {
                LogError("Some anchor can not be used");
                return;
            }

            this._isAnchorAtBottom = !this._isAnchorAtBottom;
            this._anchorAnimator.SetBool("AtBottom", this._isAnchorAtBottom);

            this.RefreshAnchorLed();
            this.SetStateForAllAnchors();

            this.UseBatteryEnergy();
        }

        private void RefreshMotorLed()
        {
            if (!this._turnedOn)
            {
                this._ledMotor.TurnOff();
                return;
            }

            if (this._isMotorActive)
            {
                this._ledMotor.TurnOn(LedBlock.Color.Green);
            }
            else
            {
                this._ledMotor.TurnOn(LedBlock.Color.Red);
                this._ledForward.TurnOff();
                this._ledBack.TurnOff();
            }
        }

        private void RefreshAnchorLed()
        {
            if (!this._turnedOn)
            {
                this._ledAnchor.TurnOff();
                return;
            }

            this._ledAnchor.TurnOn(this._isAnchorAtBottom ? LedBlock.Color.Red : LedBlock.Color.Green);
        }

        private void RefreshDirectionLed()
        {
            if (!this._turnedOn)
            {
                this._ledForward.TurnOff();
                this._ledBack.TurnOff();
                return;
            }

            switch (this._currentDirection)
            {
                case MotorDirection.Forward:
                    this._ledForward.TurnOn(this._isMotorActive ? LedBlock.Color.Green : LedBlock.Color.Red);
                    this._ledBack.TurnOff();
                    break;
                case MotorDirection.Back:
                    this._ledForward.TurnOff();
                    this._ledBack.TurnOn(this._isMotorActive ? LedBlock.Color.Green : LedBlock.Color.Red);
                    break;
            }
        }

        private void SetStateForAllMotors(bool active)
        {
            var motors = FindObjectsOfType<MotorWheel>();
            foreach (var motor in motors)
            {
                if (motor.MotorState != active)
                {
                    var message = new Message_NetworkBehaviour_ID(Messages.MotorWheel_PowerButton, this._network.Value.NetworkIDManager, motor.ObjectIndex);
                    if (Semih_Network.IsHost)
                    {
                        motor.ToggleEngine();
                        this._network.Value.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    }
                    else
                    {
                        this._network.Value.SendP2P(this._network.Value.HostID, message, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    }
                }
            }
        }

        private void StopAllMotors()
        {
            this.SetStateForAllMotors(false);
        }

        private bool DoAllMotorsActive()
        {
            var motors = FindObjectsOfType<MotorWheel>();
            if (motors.Length == 0)
            {
                LogError("no motors found");
                return false;
            }
            return motors.All(x => x.MotorState);
        }

        private void SetStateForAllAnchors()
        {
            var anchors = FindObjectsOfType<Anchor_Stationary>();
            foreach (var anchor in anchors)
            {
                if (anchor.AtBottom != this._isAnchorAtBottom && anchor.CanUse)
                {
                    var message = new Message_NetworkBehaviour(Messages.StationaryAnchorUse, anchor);
                    if (Semih_Network.IsHost)
                    {
                        AccessTools.Method("Anchor_Stationary:Use", null, null).Invoke(anchor, null);
                        this._network.Value.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    }
                    else
                    {
                        this._network.Value.SendP2P(this._network.Value.HostID, message, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    }
                }
            }
        }

        private bool CanAllAnchorsBeUsed()
        {
            var anchors = FindObjectsOfType<Anchor_Stationary>();
            if (anchors.Length == 0)
            {
                LogError("no anchors found");
                return false;
            }
            return anchors.All(x => x.CanUse);
        }

        private bool DoesAnyAnchorAtBottom()
        {
            var anchors = FindObjectsOfType<Anchor_Stationary>();
            if (anchors.Length == 0)
            {
                LogError("no anchors found");
                return false;
            }
            return anchors.Any(x => x.AtBottom);
        }

        private void InverseDirectionForAllMotors()
        {
            var motors = FindObjectsOfType<MotorWheel>();
            foreach (var motor in motors)
            {
                var message = new Message_NetworkBehaviour_ID(Messages.MotorWheel_Inverse, this._network.Value.NetworkIDManager, motor.ObjectIndex);
                if (Semih_Network.IsHost)
                {
                    motor.InversePushDirection();
                    this._network.Value.RPC(message, Target.Other, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
                    continue;
                }
                this._network.Value.SendP2P(this._network.Value.HostID, message, EP2PSend.k_EP2PSendReliable, NetworkChannel.Channel_Game);
            }
        }

        private void UseBatteryEnergy()
        {
            //this._battery.RemoveBatteryUsesNetworked(this._batteryEnergyUses);
            //if (!this._battery.CanGiveElectricity)
            //{
            //    this.HandleBatteryUpdate();
            //}
        }

        private static void Info(string message)
        {
            Debug.Log("<color=#3498db>[info]</color>\t<b>RemoteMotorControl:</b> " + message);
        }
        private static void LogError(string message)
        {
            Debug.LogError("<color=#e74c3c>[error]</color>\t<b>RemoteMotorControl:</b> " + message);
        }

        public override bool Deserialize(Message_NetworkBehaviour msg, CSteamID remoteID)
        {
            LogError($"Deserialize, msgType: {msg.Type}, remoteID: {remoteID}");
            return base.Deserialize(msg, remoteID);
        }

        public enum MotorDirection
        {
            Forward,
            Back
        }
    }
}
